## [0.21.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.20.0...v0.21.0) (2025-03-05)

### Features

* Update ansible default version in ansible-lint ([16f630c](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/16f630ce42a44cfc6b051e030e0bde744b0ffade))

## [0.20.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.19.1...v0.20.0) (2024-08-08)

### Bug Fixes

* make markdown-link-check correctly report errors ([a5eba6c](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/a5eba6c4ac34abc833720bb26536f6a9af3b77ad))

### Features

* remove specify version in semantic-release's related packages ([2c94046](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/2c94046269ef953724b383f575e2e0a20f7851fc))
* use official image in markdown-link-check ([8a7fd77](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/8a7fd7789cd0f063eb207815a79c0f78a5fe78e9))
* use rule instead only in markdown-link-check ([9301cf2](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/9301cf292b6380dd322233108a27901cff6fb500))

### CI/CD

* remove specify version in semantic-release's related packages ([a020fc7](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/a020fc77b0ad12c70e84d3a921f139fe5679c51e))

## [0.19.1](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.19.0...v0.19.1) (2024-05-24)


### Bug Fixes

* update git config in ansible-lint with install private roles ([d771ea6](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/d771ea63d7b98a162ba2dc42549cf813c9859427))

## [0.19.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.18.0...v0.19.0) (2024-05-23)


### Features

* Use shellcheck with alpine image in shellcheck. ([7515343](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/7515343d401352438975d517db793d60973e0c32))

## [0.18.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.17.0...v0.18.0) (2024-05-17)


### Bug Fixes

* downgrade conventional_changelog_conventionalcommits to 7.0.2 ([845b6cd](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/845b6cd6af7cf4e97dab085d1fecb8a61e530fb8))
* one line npm install command in semantic-release ([58b2164](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/58b21644fda02a34210c3f335527ab83da7d4d78))


### Features

* Install private ansible role in ansible-lint ([66c487a](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/66c487a999ddb46ffd17a1cb6ec4769800d2248e))
* Specify ansible version in ansible-lint ([8a7cdd9](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/8a7cdd9dcad757796b8a00d2d5ddfdc0c52e9ef3))

## [0.17.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.16.2...v0.17.0) (2024-02-22)


### Features

* Upgrade semantic-release to node 20. ([63283d3](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/63283d3f0cd1b87ffb0c81c9ced2895d4ea69626))


### CI/CD

* Upgrade CI/CD semantic-release node version to 20. ([3bfcd55](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/3bfcd55f150884e35994580423f3085d307a7fa2))

## [0.16.2](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.16.1...v0.16.2) (2023-06-12)


### Performance Improvements

* Use strict mode in ansible-lint ([a0cde57](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/a0cde578c510db4fa15172c86f1858ee35f32a31))

## [0.16.1](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.16.0...v0.16.1) (2023-06-12)


### Bug Fixes

* Install ansible package with ansible-lint ([d48d50c](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/d48d50c99ea48366659082092cf45ba218051976))

## [0.16.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.15.1...v0.16.0) (2023-06-12)


### Features

* Add ansible-lint ([470863a](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/470863a3c9e409cf76fc006a6924ee6bb7e24b17))

## [0.15.1](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.15.0...v0.15.1) (2023-06-09)


### CI/CD

* Add single quotation marks with include gitlab ci template ([a6b3ee2](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/a6b3ee2e3fccd9d87d82a2fc3e2b60bcbf19b88f))

## [0.15.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.14.1...v0.15.0) (2023-06-08)


### Features

* Update jsonlint to prantlf/jsonlint ([163081c](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/163081c3377b6d53bfc3dff9260aa1386528b031))
* Update yamllint to 1.32.0 ([095c42f](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/095c42f15fb837e5080ce3cf6668d48aeb9ca96d))
* Use editorconfig-checker in editorconfig-lint ([499d5ef](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/499d5ef822efec6d3755cba14e8facb1c6728d7e))
* Use official docker image in shellcheck ([ce11afe](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/ce11afe0aa52676fbe0380b915bda6e94e30eef4))
* Use official docker image in shfmt ([88dc727](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/88dc727a539a70ad4c7d656e1d46b381735625d0))

## [0.14.1](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.14.0...v0.14.1) (2023-06-08)


### CI/CD

* Use ci-template in master branch ([d2f45e6](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/d2f45e667991fdcfdecc20de4d353b9860e18fb5))

## [0.14.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.13.0...v0.14.0) (2023-06-08)


### Features

* Upgrade to node 18 ([b9ca803](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/b9ca80367800bf10e01779c85556d6ee24264620))

## [0.13.0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.12.2...v0.13.0) (2022-03-01)


### Features

* Update shfmt image ([723d385](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/723d385eab4e25c16e87a0a0df769911b9b61720))
* Update yamllint image ([9cc0491](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/9cc0491ae1d038d4d1c9ad55ab12c7294904b883))

### [0.12.2](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.12.1...v0.12.2) (2022-01-27)


### CI/CD

* Add editorconfig-lint ([8ad765f](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/8ad765fd1afa411a0530fe56b56bb5180d65ec43))
* Add jsonlint ([d28ceee](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/d28ceeed32c830827e05592aee2ded82b39aed3f))
* Update yamllint template version ([126dca1](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/126dca1beac10b36f701a9d8cdc599ac7310353b))

### [0.12.1](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/compare/v0.12.0...v0.12.1) (2022-01-27)


### CI/CD

* add .releaserc.json config for semantic-release ([7756d32](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/7756d32d8c7d917f241276ee6f4eba079117bc31))
* add semantic-release CI setting ([60a1d16](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/60a1d16f422254c14fbc824fe7d078e6b79cd72c))
* add semantic-release setting ([df2cb8a](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/df2cb8abbaf7e79e0ace7f549887e2e23bd64a4a))
* Add ssh-keyscan for gitlab.com ([93dd2f5](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/93dd2f52e0a6e4fc6e54e4e0f594dfe292616fbd))
* fix typo ([cb858d2](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/cb858d28c1cf1720854360797b4804c228923eea))
* Replace https:// with ssh:// ([114b707](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/114b707eeb7b01d50b5953a86f8a6ed61cc02c97))
* specify release branch ([d0ee057](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/d0ee057168ee78d18e66d7cdd099ed517a513603))
* Update semantic-release plugin ([a3613c0](https://gitlab.com/kklab-public/gitlab-ci-templates/general-template/commit/a3613c0836372161109332c65ae06f277fb49684))

# Changelogs

## v0.1.0

Features:

- 新增 [.editorconfig-lint-template](https://gitlab.kkinternal.com/gitlab-ci-template/general-template/blob/v0.1.0/editorconfig.yml)

## v0.2.0

Features:

- 新增 [.git-conflicts-detect](git-conflicts-detect.yml)
- 新增 [.shellcheck](shellcheck.yml)
- 新增 [.shfmt](shfmt.yml)
- 新增 [.yaml-lint](yaml-lint.yml)
- 新增 [.yamllint](yamllint.yml)

## v0.3.0

Breaking Changes:

- editorconfig.yml 更改檔案名稱，改成 [editorconfig-lint.yml](editorconfig-lint.yml)
- .editorconfig-lint-template 更改名稱，改成 [.editorconfig-lint](editorconfig-lint.yml)

## v0.4.0

Features:

- Add [.json-lint](json-lint.yml)

## v0.5.0

Breaking Changes:

- Update naming from json-lint to .json-lint [.json-lint](json-lint.yml)

## v0.6.0

Updates:

- Ignore .git folder in [.yamllint](test/yamllint.yml)

## v0.7.0

Fixes:

- Fix run command error when empty string sent to xargs

## v0.8.0

Fixes:

- Fix gitlab-ci template path
- Remove unused runner tags

## v0.9.0

Breaking Changes:

- Update template files docker image version
- Remove unused templates: `.yaml-lint` (duplicate of `.yamllint`)

## v0.10.0

Breaking Changes::

- Rename template json-lint to jsonlint

Updates:

- Update README.md

## v0.11.0

Features:
- Add [.markdownlint](markdownlint.yml)
- Add [.mardown-link-check](mardown-link-check.yml)
